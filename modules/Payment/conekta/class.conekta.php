<?php
if(!class_exists('\Conekta\Conekta'))
    require_once(__DIR__.'/libs/Conekta.php');

class conekta extends TokenPaymentModule implements Observer
{

    use \Components\Traits\LoggerTrait;

    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.2019-05-28';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'Conekta';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Conekta Online Checkout module';


    protected $supportedCurrencies = ['USD', 'MXN'];

    protected $configuration = [

        'Public Key' => [
            'value' => '',
            'type' => 'input'
        ],

        'Private Key' => [
            'value' => '',
            'type' => 'input'
        ],

    ];


    public function __construct()
    {

        parent::__construct();

    }


    /**
     * HostBill will run this function first time module is activated.
     * Use it to create some database tables
     */
    public function install()
    {

    }

    /**
     * HostBill will run this function when admin choose to "Uninstall" module.
     * Use it to truncate/remove tables.
     */
    public function uninstall()
    {

    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */

    public function prepareConfiguration(){
        \Conekta\Conekta::setApiKey($this->configuration['Private Key']['value']);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('en');
    }

    public function drawForm()
    {

        $ccard = HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id);
        $security_token = Tokenizer::getSalt();

        if (!$this->checkFields()) {
            return false;
        }



        if($ccard){

            $url = Utilities::url('?cmd=conekta&action=pay&security_token='.$security_token.'&invoice_id='.$this->invoice_id);


            $form ='<form action="'.$url.'" method="post" name="payform">';
            $form .= '<input type="submit" value="' . $this->paynow_text() . '" />';
            $form .= '</form>';
        }
        else {

            $url = Utilities::url('?cmd=clientarea&action=ccprocessing');

            $options['invoice_id'] = $this->invoice_id;
            $options['payment_module'] = $this->id;

            $form = '<form action="'.$url.'" method="post" name="payform">';

            foreach ($options as $key => $value) {
                $form .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
            }

            $form .= '<input type="submit" value="' . $this->paynow_text() . '" />';
            $form .='</form>';
        }
        return $form;

    }

    public function before_displayuserheader() {

        $url = Utilities::url('?cmd=conekta&action=pay');

        $r = RequestHandler::singleton();

        if ($r->getController() == 'clientarea' && $r->getAction() == 'ccprocessing' && $r->getParam('payment_module') == $this->id) {


            $module = ModuleFactory::singleton()->getModuleByFname('conekta');
            if (!$module || !($module instanceof conekta))
                return false;

            $invoice = Invoice::createInvoice($r->getParam('invoice_id'));
            $module->setCurrency($invoice->getCurrency());


            echo '<script>
                    $( document ).ready(function() {
                        
                        $("input[name=\'invoice_id\']").parent().attr(\'id\', \'card-form\' ) ;
                        $("input[name=\'cc[cardnum]\']").attr(\'data-conekta\', \'card[number]\') ;
                        $("input[name=\'cvv\']").attr(\'data-conekta\', \'card[cvc]\') ;
                        $("input[name=\'cc[expirymonth]\']").attr(\'data-conekta\', \'card[exp_month]\') ;
                        $("input[name=\'cc[expiryyear]\']").attr(\'data-conekta\', \'card[exp_year]\') ;
                        $("input[name=\'cc[expiryyear]\']").attr(\'maxlength\', \'4\') ;
                        $("input[name=\'cc[expiryyear]\']").attr(\'minlength\', \'4\') ;
                                            
                        
                     });
                 </script>';

            echo '<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>';
            echo '<script type="text/javascript">
                      Conekta.setLanguage("en");   
                      Conekta.setPublicKey(\''.$this->configuration['Public Key']['value'].'\');
                    
                      var conektaSuccessResponseHandler = function(token) {
                        var $form = $("#card-form");
                        //Add the token_id in the form
                        $form.append($(\'<input type="hidden" name="conektaTokenId" id="conektaTokenId">\').val(token.id));
                        $form.attr(\'action\', \''.$url.'\') ;
                        $form.get(0).submit(); //Submit
                      };
                      var conektaErrorResponseHandler = function(response) {
                        var $form = $("#card-form");
                        
                        $form.find(".card-errors").text(response.message_to_purchaser);
                        $form.find("button").prop("disabled", false);
                      };
                    
                      //jQuery generate the token on submit.
                      $(function () {

                        $("#card-form").submit(function(event) {
                          var firstname  = $("input[name=\'client[firstname]\']").val();
                          var lastname  = $("input[name=\'client[lastname]\']").val();
                          var name =  firstname + \' \'+ lastname;
                          
                           //$("#card-form").append($(\'<input type="hidden" name="card_name" data-conekta="card[name]"> \').val(name));  
                            
                          var $form = $(this);
                          $form.append($(\'<input type="hidden" name="card_name" data-conekta="card[name]"> \').val(name));  
                          // Prevents double clic
                          $form.find("button").prop("disabled", true);
                          Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                          return false;
                        });
                      });
                    </script>';


        }
    }

    public function capture_token($params){


        if($params['conektaTokenId']) {

            $this->prepareConfiguration();
            $invoice = Invoice::createInvoice($this->invoice_id);

            $customerInfo = [
                "name" => $params['card_name'],
                "email" => $this->client['email'],
                "phone" => $this->client['phonenumber'],
                "payment_sources" => [
                    [
                        "type" => "card",
                        "token_id" => $params['conektaTokenId']
                    ]
                ]
            ];

            try {
                $customer = \Conekta\Customer::create($customerInfo);
                $source = $customer->payment_sources[0];
                $customer->update([ 'default_payment_source_id' => $source->id]);

            } catch (\Conekta\ProcessingError $error) {

                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;

            } catch (\Conekta\ParameterValidationError $error) {
                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;

            } catch (\Conekta\Handler $error) {
                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;
            }


            $lineItems = [];
            $lineItems[] = [
                'name' => 'Invoice: ' . $this->invoice_id,
                'unit_price' => (string)($invoice->getTotal() * 100),
                'quantity' => 1
            ];

            $charges = [];
            $charges[] = ["payment_method" => ["type" => "default"]];

            $newCustomerInfo = ["customer_id" => $customer->id];

            try {
                $order = \Conekta\Order::create([
                    'currency' => $this->currency_code,
                    'customer_info' => $newCustomerInfo,
                    'line_items' => $lineItems,
                    'charges' => $charges
                ]);


            } catch (\Conekta\ProcessingError $error) {

                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;

            } catch (\Conekta\ParameterValidationError $error) {
                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;

            } catch (\Conekta\Handler $error) {
                $this->logActivity([
                    'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");
                return false;
            }

            if ($order->payment_status == 'paid') {


                $charges = (array)$order->charges;
                $charge = $charges[0];
                $payment_method = $charge->payment_method;

                $transaction_params = [
                    'client_id' => $this->client['id'],
                    'invoice_id' => $this->invoice_id,
                    'description' => $order->id,
                    'transaction_id' => $order->id,
                    'in' => $invoice->getTotal(),
                    'fee' => '0'
                ];

                $this->logActivity([
                    'output' => $order,
                    'result' => self::PAYMENT_SUCCESS
                ]);


                if (!HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id)) {
                    HBLoader::LoadModel('Clients/CreditCardsUtils')->TokenizeNotStoredCC($this->client['id'], $payment_method->last4, $customer->id, $this->id);

                }

                $this->addTransaction($transaction_params);

                return true;
            } else {
                $this->logActivity([
                    'output' => ['error' => $order ? 'Payment status: ' . $order->payment_status : 'Unknown error'],
                    'result' => self::PAYMENT_FAILURE
                ]);

                $this->addError("Payment failed please contact administrator");
            }

            return false;
        }

        if(HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id))
            return $this->collect($params);
        else
            return false;

    }

    /**
     * Handle payment refund
     *
     * @param $_transaction
     */
    public function refund($_transaction, $amount) {

        $this->prepareConfiguration();

        $query = $this->db->prepare("SELECT `description` FROM hb_transactions WHERE `trans_id` = :trans_id AND `module` = :module_id");
        $query->execute([
            'trans_id' => $_transaction,
            'module_id' => $this->getModuleId()
        ]);
        $ref = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();


        try{
            $order = \Conekta\Order::find($ref['description']);
            $refund = $order->refund([
                'reason' => 'requested_by_client',
                'amount' => (string)($amount * 100)
            ]);
        }catch (\Conekta\ProcessingError $error) {

            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;

        } catch (\Conekta\ParameterValidationError $error) {
            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;

        } catch (\Conekta\Handler $error) {
            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;
        }



        if (in_array($order->payment_status, ['refunded', 'partially_refunded'])){

            $result = $this->addRefund(array(
                'target_transaction_number' => $_transaction,
                'transaction_id' => $order->id,
                'amount' => $amount
            ));
            $this->logActivity(array(
                'output' =>  $_transaction,
                'result' =>  self::PAYMENT_SUCCESS
            ));
            return $result;
        }

        $this->logActivity(array(
            'output' => 'Faild to create refund: '.$order,
            'result' => PaymentModule::PAYMENT_FAILURE
        ));

        return false;

    }

    public function collect($params){

        $this->prepareConfiguration();

        $ccard = HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id);


        $invoice = Invoice::createInvoice($params['invoice_id']);

        $lineItems = [];
        $lineItems[] = [
            'name' => 'Invoice: ' . $params['invoice_id'],
            'unit_price' => (string)($invoice->getTotal() * 100),
            'quantity' => 1
        ];

        $charges = [];
        $charges[] = ["payment_method" => ["type" => "default"]];

        $customerInfo = ["customer_id" => $ccard['token']];

        try {
            $order = \Conekta\Order::create([
                'currency' => $this->currency_code,
                'customer_info' => $customerInfo,
                'line_items' => $lineItems,
                'charges' => $charges
            ]);


        } catch (\Conekta\ProcessingError $error) {

            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;

        } catch (\Conekta\ParameterValidationError $error) {
            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;

        } catch (\Conekta\Handler $error) {
            $this->logActivity([
                'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");
            return false;
        }

        if ($order->payment_status == 'paid') {

            $transaction_params = [
                'client_id' => $this->client['id'],
                'invoice_id' => $this->invoice_id,
                'description' => $order->id,
                'transaction_id' => $order->id,
                'in' => $invoice->getTotal(),
                'fee' => '0'
            ];

            $this->logActivity([
                'output' => $order,
                'result' => self::PAYMENT_SUCCESS
            ]);


            $this->addTransaction($transaction_params);

            return true;
        } else {
            $this->logActivity([
                'output' => ['error' => $order ? 'Payment status: ' . $order->payment_status : 'Unknown error'],
                'result' => self::PAYMENT_FAILURE
            ]);

            $this->addError("Payment failed please contact administrator");
        }


        return false;

    }

    public function getCartHTML() {
        return "";
    }
}







