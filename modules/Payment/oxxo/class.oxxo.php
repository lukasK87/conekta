<?php

if(!class_exists('\Conekta\Conekta'))
    require_once(__DIR__.'/../conekta/libs/Conekta.php');

class oxxo extends PaymentModule
{

    use \Components\Traits\LoggerTrait;

    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.2019-05-28';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'Oxxo';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Oxxo payment module';


    protected $supportedCurrencies = ['USD', 'MXN'];

    protected $configuration = [

        'Public Key' => [
            'value' => '',
            'type' => 'input'
        ],

        'Private Key' => [
            'value' => '',
            'type' => 'input'
        ],

    ];


    public function __construct()
    {

        parent::__construct();

    }


    /**
     * HostBill will run this function first time module is activated.
     * Use it to create some database tables
     */
    public function install()
    {

    }

    /**
     * HostBill will run this function when admin choose to "Uninstall" module.
     * Use it to truncate/remove tables.
     */
    public function uninstall()
    {

    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */

    public function prepareConfiguration(){
        \Conekta\Conekta::setApiKey($this->configuration['Private Key']['value']);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('en');
    }

    public function drawForm()
    {


        $security_token = Tokenizer::getSalt();

        if (!$this->checkFields()) {
            return false;
        }

        $form ='<form action="'.HBConfig::getConfig('InstallURL').'?cmd=oxxo&action=pay&security_token='.$security_token.'&invoice_id='.$this->invoice_id.'" method="post" name="payform">';
        $form .= '<input type="submit" value="' . $this->paynow_text() . '" />';
        $form .= '</form>';

        return $form;

    }



    public function generateStub(){

        $this->prepareConfiguration();
        $invoice = Invoice::createInvoice($this->invoice_id);


        $customerInfo = [
            "name" =>  $this->client['firstname'].' '.$this->client['lastname'],
            "email" => $this->client['email'],
            "phone" => $this->client['phonenumber'],
        ];


        $lineItems = [];
        $lineItems[] = [
            'name' => 'Invoice: ' . $this->invoice_id,
            'unit_price' => (string)($invoice->getTotal() * 100),
            'quantity' => 1
        ];

        try {
            $order = \Conekta\Order::create(
                [
                    "line_items" => $lineItems,
                    "currency" => $this->currency_code,
                    "customer_info" => $customerInfo,
                    "charges" => [["payment_method" => ["type" => "oxxo_cash"]]]]
            );

        } catch (\Conekta\ProcessingError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\ParameterValidationError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\Handler $error) {
            return $this->errorLog($error);
        }

        $reference = $order->charges[0]->payment_method->reference;
        $amount = $order->line_items[0]->unit_price/100;

        return ['reference' => $reference, 'amount' => number_format($amount, 2), 'currency_code' =>$this->currency_code, 'base_url'  => HBConfig::getConfig('InstallURL')];
    }

    public function callback(){

        $body = @file_get_contents('php://input');
        $this->logger()->info("callback oxo",[$body]);
        $data = json_decode($body, true);
        http_response_code(200); // Return 200 OK

        //$log = $data;
        $result = self::PAYMENT_FAILURE;

        if($data['type']!='charge.paid')
            return false;
        $oxxo_id = $data['data']['object']['order_id'];

        $this->prepareConfiguration();

        try{
            $order = \Conekta\Order::find($oxxo_id);

        }catch (\Conekta\ProcessingError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\ParameterValidationError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\Handler $error){
            return $this->errorLog($error);
        }

        if($order->payment_status=='paid'){
            $name = $order->line_items[0]->name;
            $amount = $order->line_items[0]->unit_price/100;
            $invoice_id = str_replace('Invoice: ', '', $name);

            $invoice = Invoice::createInvoice($invoice_id);

            if($invoice->getInvoiceId() != $invoice_id){
                $log = 'Invalid Invoice no.: '.$invoice_id;
            }
            else{
                $result = self::PAYMENT_SUCCESS;

                $transaction_id = $oxxo_id;

                if (!$this->_transactionExists($transaction_id)) {
                    $this->addTransaction([
                            'invoice_id' => $invoice_id,
                            'client_id' => $invoice->getClientId(),
                            'in' => $amount,
                            'transaction_id' => $transaction_id,
                            'description' => 'OXXO Payment ' . $invoice_id.' Total paid: '.$amount,
                            'fee' => 0
                    ]);
                    }
                    $log['transaction'] = $transaction_id;
                }
            }
            else{
                $log = 'Invalid order payment status: '.$oxxo_id;
            }


        $this->logActivity([
            'output' => $log,
            'result' => $result
        ]);

    }


    public function errorLog($error){
        $this->logActivity([
            'output' => ['error' => $error ? $error->getMessage() : 'Network connection problem'],
            'result' => self::PAYMENT_FAILURE
        ]);
        $this->addError("Payment failed please contact administrator");

        return false;
    }



    /**
     * Handle payment refund
     *
     * @param $_transaction
     */


    public function refund($_transaction, $amount) {

        $this->prepareConfiguration();

        $query = $this->db->prepare("SELECT `description` FROM hb_transactions WHERE `trans_id` = :trans_id AND `module` = :module_id");
        $query->execute([
            'trans_id' => $_transaction,
            'module_id' => $this->getModuleId()
        ]);
        $ref = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();


        try{
            $order = \Conekta\Order::find($ref['description']);
            $refund = $order->refund([
                'reason' => 'requested_by_client',
                'amount' => (string)($amount * 100)
            ]);
        }catch (\Conekta\ProcessingError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\ParameterValidationError $error) {
            return $this->errorLog($error);
        } catch (\Conekta\Handler $error) {
            return $this->errorLog($error);
        }



        if (in_array($order->payment_status, ['refunded', 'partially_refunded'])){

            $result = $this->addRefund(array(
                'target_transaction_number' => $_transaction,
                'transaction_id' => $order->id,
                'amount' => $amount
            ));
            $this->logActivity(array(
                'output' =>  $_transaction,
                'result' =>  self::PAYMENT_SUCCESS
            ));
            return $result;
        }

        $this->logActivity(array(
                'output' => 'Faild to create refund: '.$order,
                'result' => PaymentModule::PAYMENT_FAILURE
        ));

        return false;

    }
    




}





